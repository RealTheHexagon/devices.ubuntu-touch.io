/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const axios = require("axios");
const requireMaybe = require("require-maybe");
const slugify = require("@sindresorhus/slugify");

const portStatus = require("./data/portStatus.json");
const deviceInfo = require("./data/deviceInfo.json");
const progressStages = require("./data/progressStages.json");
const installerData = requireMaybe("./data/installerData.json") || [];

// Display a warning if installer data wasn't downloaded
if (!installerData.length) {
  console.log(
    "\x1b[33m%s\x1b[0m",
    "Installer data unavailable at build time.\n",
    "You should consider running one of the following commands instead:\n",
    "npm run develop : Fetch data and run in development mode\n",
    "npm run build : Fetch data and build site for production mode\n",
    "npm run get-data : Only fetch data"
  );
}

module.exports = function (api) {
  api.loadSource(async (actions) => {
    const netlifyRedirects = actions.addCollection("NetlifyRedirects");

    async function getAssetUrl(packageType) {
      try {
        const results = await axios.get(
          "https://api.github.com/repos/ubports/ubports-installer/releases/latest"
        );
        return results.data.assets.find((asset) =>
          asset.name.toLowerCase().endsWith(packageType.toLowerCase())
        ).browser_download_url;
      } catch (error) {
        console.log(error);
        return "/installer";
      }
    }

    for (let el of ["exe", "deb", "dmg", "appimage"]) {
      netlifyRedirects.addNode({
        from: "/installer/" + el,
        to: await getAssetUrl(el),
        status: 302
      });
    }

    netlifyRedirects.addNode({
      from: "/installer/snap",
      to: "https://snapcraft.io/ubports-installer",
      status: 302
    });
  });

  api.onCreateNode((options) => {
    if (options.internal.typeName === "Device" && !options.elaborated) {
      // Set codename from file name
      options.codename = options.fileInfo.name;

      // Calculate porting progress from feature matrix ( use maturity field as fallback )
      if (options.portStatus) {
        let featureScale = ["x", "-", "?", "+-", "+"],
          notFound = "",
          totalWeight = 0,
          currentWeight = 0,
          currentStageIndex = progressStages.length - 1; // Daily-driver ready

        for (let portCategory in portStatus) {
          // Get category from collection
          let category = options.portStatus.find(
            (el) => el.categoryName == portCategory
          );

          for (let feature of portStatus[portCategory]) {
            // Get feature from collection
            let graphQlFeature = category.features.find(
              (el) => el.id == feature.id
            );

            if (graphQlFeature) {
              // Replace ID with name
              graphQlFeature.name = feature.name;
              delete graphQlFeature.id;

              // Delete unavailable features
              if (graphQlFeature.value == "x") {
                category.features.splice(
                  category.features.indexOf(graphQlFeature),
                  1
                );
                continue;
              }
            } else {
              notFound += feature.name + "\n";

              // Don't add unavailable features
              if (feature.default == "x") {
                continue;
              }

              // Add missing features from defaults
              category.features.push({
                name: feature.name,
                value: feature.default ? feature.default : "?"
              });

              // Get feature from collection
              graphQlFeature = category.features.find(
                (el) => el.name == feature.name
              );
            }

            // Check global state of the feature
            if (
              feature.global &&
              featureScale.indexOf(feature.global) <=
                featureScale.indexOf(graphQlFeature.value)
            ) {
              graphQlFeature.value = feature.global;
              graphQlFeature.bugTracker = feature.bugTracker
                ? feature.bugTracker
                : "";
              graphQlFeature.global = true;
            }

            // Calculate progress (from weights)
            totalWeight += feature.weight;
            currentWeight += graphQlFeature.value == "+" ? feature.weight : 0;

            // Calculate progress stage
            if (graphQlFeature.value != "+") {
              let featureStageIndex = progressStages.indexOf(feature.stage);
              if (
                currentStageIndex >= featureStageIndex &&
                featureStageIndex > 0
              ) {
                currentStageIndex = featureStageIndex - 1;
              }
            }
          }
          if (category.features.length == 0) {
            options.portStatus.splice(options.portStatus.indexOf(category), 1);
          }
        }

        // Display missing feature warning
        if (notFound.length > 0) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            options.name + " has missing features in feature matrix.",
            "\nThe following features are missing:\n" + notFound
          );
        }

        // Calculate progress
        options.progress = currentWeight / totalWeight;
        options.progressStage = progressStages[currentStageIndex];
      } else {
        // Fallback from maturity
        options.progress = options.maturity;
        options.progressStage = progressStages[0];
        console.log(
          "\x1b[33m%s\x1b[0m",
          "Using maturity as fallback for: " + options.name
        );
      }

      // Elaborate device info from IDs
      if (options.deviceInfo) {
        options.deviceInfo.forEach((el) => {
          el.name = deviceInfo.find((info) => info.id == el.id).name;
          delete el.id;
        });
      }

      // Import installer compatibility data
      let deviceCodenameAlias = options.installerAlias
        ? options.installerAlias
        : options.codename;
      let deviceInstaller = installerData.some(
        (el) =>
          el.codename == deviceCodenameAlias &&
          el.operating_systems.includes("Ubuntu Touch")
      );
      options.noInstall = !deviceInstaller;

      // Add weight to installer and port data
      options.progress = options.noInstall
        ? options.progress * 0.9
        : options.progress;
      options.progress = options.portStatus
        ? options.progress
        : options.progress * 0.95;
      options.progress = Math.round(1000 * options.progress) / 10;
      options.elaborated = true;
    }
  });

  api.createPages(async ({ graphql, createPage }) => {
    const { data } = await graphql(
      "{ allDevice { edges { node { id codename aliases path } } } }"
    );

    data.allDevice.edges.forEach(({ node }) => {
      for (let alias of node.aliases) {
        createPage({
          path: "/device/" + slugify(alias, { decamelize: false }),
          component: "./src/templates/Device.vue",
          context: {
            id: node.id
          }
        });
      }
    });
  });
};
