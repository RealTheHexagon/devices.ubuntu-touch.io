const Fs = require("fs");
const Path = require("path");
const Axios = require("axios");

async function getInstallerData() {
  console.log("Downloading data...");

  const response = await Axios({
    url: "https://ubports.github.io/installer-configs/v2/",
    method: "GET",
    responseType: "stream"
  })
    .then((response) => {
      const writer = Fs.createWriteStream(
        Path.resolve(__dirname, "../data", "installerData.json")
      );

      response.data.pipe(writer);

      writer.on("finish", () => {
        console.log("Download complete!");
      });

      writer.on("error", () => {
        console.log("\x1b[31m%s\x1b[0m", "Download failed!");
      });
    })
    .catch((error) => {
      console.log("\x1b[31m%s\x1b[0m", "Download failed!");
    });
}

getInstallerData();
